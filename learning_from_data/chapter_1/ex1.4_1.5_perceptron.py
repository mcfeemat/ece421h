import numpy as np


class Perceptron:
    def __init__(self, x, y, max_iter, lr=1, ada=False):
        self.x = x
        self.y = y
        self.w = np.ones((x.shape[1]))
        self.max_iter = max_iter
        self.lr = 0.01
        self.current_iter = 1

    def predict(self, x):
        predictions = np.dot(x, self.w)

        predictions[predictions > 0] = 1
        predictions[predictions < 0] = -1

        return predictions

    def _update_weights(self, x_n, y_n):
        self.w = self.w + self.lr * y_n * x_n

    def _calculate_error(self, predictions):
        error = np.sum(predictions != self.y)
        return error

    def train(self):
        while self.current_iter < self.max_iter:  # Check if reached max iterations
            predictions = self.predict(self.x)
            self.current_error = self._calculate_error(predictions)
            if self.current_error == 0:
                break
            for i, row in enumerate(predictions):  # Find a misclassified example
                if predictions[i] != self.y[i]:
                    self._update_weights(self.x[i], self.y[i])
                    break  # Stop once a misclassified example is found
            self.current_iter += 1


# From https://stackoverflow.com/questions/47961536/how-to-generate-linear-separable-dataset-by-using-sklearn-datasets-make-classifi
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification

separable = False
while not separable:
    samples = make_classification(
        n_samples=100,
        n_features=2,
        n_redundant=0,
        n_informative=1,
        n_clusters_per_class=1,
        flip_y=-1,
        random_state=1,
    )
    red = samples[0][samples[1] == 0]
    blue = samples[0][samples[1] == 1]
    separable = any(
        [
            red[:, k].max() < blue[:, k].min() or red[:, k].min() > blue[:, k].max()
            for k in range(2)
        ]
    )
plt.plot(red[:, 0], red[:, 1], "r.")
plt.plot(blue[:, 0], blue[:, 1], "b.")
plt.show()

x = samples

x = samples[0]
ones = np.ones((x.shape[0], 1))
x = np.concatenate((x, ones), axis=1)
y = samples[1]
y[y == 0] = -1

perceptron = Perceptron(x, y, 2000)
perceptron.train()

outputs = perceptron.predict(x)


np.sum(outputs == y)

perceptron.current_iter


class Adaline:
    def __init__(self, x, y, max_iter, lr=1, ada=False):
        self.x = x
        self.y = y
        self.w = np.ones((x.shape[1]))
        self.max_iter = max_iter
        self.lr = 0.01
        self.current_iter = 1

    def predict(self, x):
        signal = np.dot(x, self.w)
        predictions = signal
        predictions[predictions > 0] = 1
        predictions[predictions < 0] = -1

        return signal, predictions

    def _update_weights(self, x_n, y_n, s_n):
        self.w = self.w + self.lr * (y_n - s_n) * x_n

    def _calculate_error(self, predictions):
        error = np.sum(predictions != self.y)
        return error

    def train(self):
        while self.current_iter < self.max_iter:  # Check if reached max iterations
            signal, predictions = self.predict(self.x)
            self.current_error = self._calculate_error(predictions)
            if self.current_error == 0:
                break
            for i, row in enumerate(signal):  # Find a misclassified example
                if signal[i] * self.y[i] < 0:
                    self._update_weights(self.x[i], self.y[i], signal[i])
                    break  # Stop once a misclassified example is found
            self.current_iter += 1


adaline = Adaline(x, y, 2000)
adaline.train()
outputs = perceptron.predict(x)
print(np.sum(outputs == y))
print(adaline.current_iter)
