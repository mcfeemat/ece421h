\documentclass[12pt]{article}
\usepackage{times}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage[margin=0.75in]{geometry}
\usepackage{float}
\usepackage{subfigure}
% \usepackage[superscript,biblabel]{cite}

\begin{document}
\noindent
Author: Matthew McFee\\
Student \#: 1005449631\\
Date: Jan. 31, 2022

\section{Logistic Regression with Numpy}

\subsection{1. Loss Function and Gradient}
Implementing the loss function was simple, I just assured that the arrays of labels and
predictions were flat and then wrote out the equation as specified in the homework
document. Numpy automatically broadcasts/computes the quantity element wise. To
implement the \verb|grad_loss| function I had to calculate the gradient of the loss function
which involved computing each of the derivatives of the loss function with respect to
each weight. The derivation process is shown in Figure 1 below. The code snippet showing
my implementation is shown in Figure 2.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.60\linewidth]{q1_1.jpg}
  \caption[Derivation of the gradient]{Calculation of the derivatives of the loss with
    respect to the weights to generate the gradients for use with gradient descent.}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\linewidth]{q1_1/loss_and_grad.png}
  \caption[Code snippet for loss and gradient]{The analytical loss and gradients for the
  logistic regression implemented in Numpy.}
\end{figure}

\subsection{Gradient Descent Implementation}
The gradient descent algorithm was implemented as specified in the course textbook
``Learning From Data.''  I included inputs to the started function for validation data
so that I could properly track the losses during training of the model. The
implementation is also shown in the figure below.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.65\linewidth]{q1_2/grad_descent.png}
  \caption[Code snippet for gradient descent implementation]{An implementation of
    gradient descent in Numpy.}
\end{figure}

\subsection{Tuning the Learning Rate}
Recall that the gradient is the direction of steepest
ascent and thus increasing error, so we want to move in the opposite direction. The
learning rate controls how large of a step we take in opposite direction of the
gradient. We want to select a learning rate that is large enough that the
model does not update very slowly and take a long time to converge. Similarily, we do
not want to select a learning rate that is so large that the weights update erratically
or diverge. In the case of the tested rates 0.005, 0.001, 0.0001 the final accuracies
are 0.98, 0.98, 0.99. The corresponding losses are 0.06016134696986814,
0.08372199750880133, 0.1770087650593534. The learning rate of 0.0001 seems to
converge too slowly. The learning rate of 0.01 converges similarly to 0.005 but to a
slightly lower validation accuracy. Thus, the best learning rate is likely 0.005 as it reaches a
low loss/validation accuracy fastest.

\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.005_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.005_accuracy.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.001_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.001_accuracy}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.0001_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_3/learning_rate_0.0001_accuracy}}
    \caption[Tuning the learning rate]{Different loss and accuracy curves for each
      specified learning rate.}
\end{figure}

\subsection{Generalization}
The function of regularization terms in the loss function is to sacrifice performance on
the training set in exchange for increased generalizabiltiy on the validation set. In
other words, the model may be overfitting to the data and generalizing poorly to the
validation data that is unseen by the model. L2 regularization attempts to keep the
model weights as small as possible and thus attempts to push weights for irrelevant
features to 0 preventing the fitting of noise. All regularization values 0.001, 0.1, 0.5
reach accuracies of 0.98, 0.98, and 0.99 with corresponding losses 0.061497681205948666,
0.12664707409254491, 0.21539388160775458. The training set seems to very similar to the
test set, and thus regularization does not seem to have a large impact. So, I would use
a regularization value of 0.001 since it is the most conservative value, has the best
loss, and a good accuracy.
\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.001_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.001_accuracy.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.1_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.1_accuracy}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.5_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q1_4/regularization_0.5_accuracy}}
    \caption[Tuning the regularization]{Different loss and accuracy curves for each
      specified regularization value.}
\end{figure}

\section{Logistic Regression in TensorFlow}

\subsection{Building the Computational Graph}
I implemented my solution in TensorFlow2 so I do not need to implement a computational
graph. Instead, I implemented a logistic regression class that has methods for computing
the loss as specified, doing the forward pass, as well as a training method that
implements SGD. I also implemented batching with the \verb|tf.split()| function and
shuffled the data using a method that randomizes the indices and then regathers the
tensor with the specified randomized order. The weigh vector \verb|w| was initalized
with a specified random seed for better reproducability of results. The Adam optimizer built
into the \verb|keras| library of \verb|tf| was used for optimization. Please, note that
I have called the \verb|buildGraph()| function \verb|forward()| in my implementation for
notational clarity and appropriateness.

\subsection{Implementing Stochastic Gradient Descent}
I implemented stochastic gradient descent with \verb|tf.GradientTape()|. During the
forward pass the gradients are computed with respect to the loss function. I then can
simply call the optimizer function with the gradients and it peforms the
backpropagation. The training curves for the default parameters are available in the
figure below.

\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_2/tf_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_2/tf_acc.png}} 
    \caption[Tensorflow implementation]{Loss and accuracy for my TensorFlow2
      implementation of logistic regression with the specified default parameters.}
\end{figure}

\subsection{Batch Size Investigation}
The batch size is the number of examples we are passing through the forward pass of the
model and then using to compute the average loss. Thus, the batch
is approximating the optimal gradient and thus the direction to move the weights in to
reduce the loss. The greater the number of examples in the batch, the more accurate the
approximation is but the more computationally/resource intensive the computation
is. If the batch size is relatively small the approximation is less accurate and the
training is more noisey. Therefore, we want to pick a batch size that is not too small
or too large such that the model trains stabily, quickly, and without using too many
resources. For the batch sizes of 100, 700, and 1750 the accuracies are 0.98, 0.97, 0.97
and the corresponding losses are 0.06388747, 0.08508437, 0.05814393.
In this case all batch sizes result in similar losses. However, 100 is very
noisey and 1750 is quite large. A good intermediate is 500, or 700. It is also important
to note that the larger batch sizes converge faster. It is likely that
the batch size has a relatively small impact on the final accuracy as many of the
examples are quite similar to each other as we are only dealing with two characters in
this case.

\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_100_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_100_accuracy.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_700_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_700_accuracy}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_1750_loss.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_3/batch_size_1750_accuracy}}
    \caption[Tuning the batch size]{Different loss and accuracy curves for each
      specified batch size.}
 \end{figure}

 \subsection{Hyperparameter Investigation}
 Hyperparameters are use tuned parameters that the model does not learn. Often, we need
 to try many different combinations of hyperparameters in an attempt maximize the final
 perfomance of the model. Beta 1 is the exponential decay rate for moment 1, beta 2 is the same for moment 2, and
 epsilon prevents division by zero during calculations for numerical stability. For beta
 1's values of 0.95 and 0.99 the validation accuracies are 0.98 and 0.99 with losses 0.09510855,
 0.04510868. For the
 beta 2 values of 0.99 and 0.9999 the final validation accuracies are 0.96 and
 0.98 with losses 0.085980065, 0.12044364. For the episilon values of 0.0001 and
 0.000000001. The final accuracies are both 0.97 and 0.98 with losses 0.097770624, 0.039829794. Thus,
 I would pick 0.99 for beta 1 since it has lowest loss and highest accuracy, 0.9999 for
 beta 2 since it has higher accuracy and only slightly higher loss, and 0.0001 for
 epsilon since it has higher accuracy and lower loss.
 %This is also the case for the other
 % two sets of parameters. Thus, it would be safe to pick any configuration of these
 % parameters. Each of the parameters is very close to the defaults in Keras. Where the
 % beta 1, beta 2, and epsilon defaults are 0.9, 0.99, $1 * 10^-7$ so it makes sense that
 % the selected betas give very similar results.
 \subsubsection{Beta 1}
 \begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta1_0.95_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta1_0.95_accuracy.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta1_0.99_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta1_0.99_accuracy.png}} 
    \caption[Tuning beta_1]{Different loss and accuracy curves for each
      specified beta_1 value.}
 \end{figure}


 \subsubsection{Beta 2}
 \begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta2_0.99_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta2_0.99_accuracy.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta2_0.9999_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/beta2_0.9999_accuracy.png}} 
    \caption[Tuning beta_2]{Different loss and accuracy curves for each
      specified beta_2 value.}
 \end{figure}
 
 \subsubsection{Epsilon}
\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/epsilon_1e-09_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/epsilon_1e-09_accuracy.png}}
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/epsilon_0.0001_loss.png}} 
    \subfigure[]{\includegraphics[width=0.33\textwidth]{q2_4/epsilon_0.0001_accuracy.png}} 
    \caption[Tuning epsilon]{Different loss and accuracy curves for each
      specified epsilon value.}
  \end{figure}
  
 \subsection{Comparison to Batch GD}
 In comparison to the batch gradient descent the Adam optimizer results in significantly faster
 training in comparison to Batch GD. For example, in the original Numpy batch gradient
 descent it took 5000 epochs (or total passes through the data) to converge
 vs. $(500/3500) * 5000 = 714$ epochs to converge for batch size $500$. This is because Adam
 keeps track of previous gradient values uses a weighted sum of past values and the
 current gradient for the current iteration. Thus, if moving the weights in a certain
 direction has been historically good (reducing the loss) the weight updates will be
 larger in that given direction. Thus, Adam possesses a gradient ``momentum'' that
 allows it to converge more quickly.

\begin{figure}[H]
    \centering
    \subfigure[]{\includegraphics[width=0.5\textwidth]{q2_5/numpy_batch_gd.png}} 
    \subfigure[]{\includegraphics[width=0.5\textwidth]{q2_5/tf_adam_loss.png}} 
    \caption[Batch GD vs. Adam]{Different loss and accuracy curves for both Batch GD/Numpy and Tensorflow2/Adam.}
 \end{figure}

\end{document}

