\documentclass[12pt]{article}
\usepackage{times}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage[margin=0.75in]{geometry}
% \usepackage[superscript,biblabel]{cite}

\begin{document}
\noindent
\textbf{A Deep Learning Based Scoring Function To Accelerate
  Computational Antibody Design: }
Protein-protein complexes play a vital role in processes such as mediating biochemical
reactions, signal transduction, and the immune response in the form of antigen-antibody interactions. The key methodology to study
proteins interactions involves using techniques such as
x-ray crystallography to determine three dimensional structures. However,
these techniques are often difficult and time consuming, which has encouraged the development
of in silico methods for solving tasks involving protein structure and interaction. For example,
computationally designing the complementary determining regions (CDRs) of an antibody to
strongly bind a specific epitope. These computational methods are dependent on
scoring functions to determine the energetic favourability of the configuration of
interest. Unfortunately, previously designed scoring functions
often perform poorly due to the challenges of hand-designing the terms of the
equations such that they capture all of the biophysical complexity
of the system \cite{moal2013, huang2010}. Deep learning has
recently exploded in popularity and has shown incredible
promise in solving difficult problems in protein biology with far greater
accuracy than traditional methods \cite{gao2020,
  strokach2020, jumper2021}.
\textbf{The goal of this project is to develop a deep learning model that can accurately
  score protein complex quality and use this model to accelerate the development of targeted
  antibody treatment by screening computational designs.} The specific
aims are as follows:

\noindent
\textbf{Aim 1: Develop a graph-based model architecture that learns to accurately
  score protein-protein complexes: }
Many deep learning scoring function models are based on mathematical convolutions
\cite{wang2019, mcnutt2021} which require data preprocessing that results in loss
structural information or variability in data due to coordinate rotations and
translations. Representing protein complexes as mathematical graphs
and using graph based embedding techniques addresses these limitations. My
model architecture operates on protein graphs and is based on a previously successful
networks \cite{ingraham2019, abdin2021}. This model also incorporates a novel
bidirectional exchange layer which allows for information to flow between the proteins
during learning. We have shown that this model can learn meaningful information from my
generated training set that can be used to predict complex quality. The model performance has been compared to the two top performing
deep learning models in the literature \cite{geng2019, wang2021}. My model outperforms these models
in multiple assessment metrics, however it fails to rank the best
structures in the top ten ranks as effectively as the
other models. To address this, we generated an improved dataset which is significantly
more diverse and representative than previously used small, gold-standard scoring
function training sets \cite{hwang2010, lensink2014, kundrotas2017}.

\noindent
\textbf{Aim 2: Extend the model architecture to design antibody CDRs for specific
  epitopes: }
Considering the model has thus far been able to learn meaningful protein-complex
embeddings, we intend to adapt the scoring model to instead predict missing protein structure. This will
be done by training the model to predict all atom coordinates for amino acids that have
had their structural features in the graph masked. We will then combine this model with a
natural language processing model trained on antibody sequence information to design antibodies
in a iterative, one-at-a-time fashion. The natural language model, which has been
trained on a repertoire of antibody sequences to mine important sequence trends, will
act as a constraint and predict potential next amino acids and the generative model will predict the associated coordinates.
Furthermore, we can assess a design's energetic favourability with my scoring model which
will be used to select the most favourable amino acid.

\noindent
\textbf{Aim 3: Validate the model in vivo and employ the models for end-to-end CDR
  design: }
Previously, we have worked on a project involving the computational design of the
CDRS of antibodies for specific targets including PD-1 on the surface of T cells. To
assess the performance of my model we plan to rerank
the outputs of this design pipeline to determine enrichment of performant
binders. Furthermore, we will generate antibody CDR loops for a new target (such as CXCR4,
an alpha-chemokine receptor) and then validate the top scorers produced by our model in
vivo to show that it does create loops that bind the target effectively. In vivo
validations will include ELISA
and surface plasmon resonance assays to determine binding affinities. Thus, my models will be
validated as a complete package to design antibodies.

\newpage
\bibliographystyle{IEEEtran}
\bibliography{bib}

\end{document}